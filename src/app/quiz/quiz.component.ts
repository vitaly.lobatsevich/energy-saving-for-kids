import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Quiz } from '../entities/quiz';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  @Input() quiz: Quiz = {
    firstImage: {
      source: '',
      alternative: 'Первое изображение'
    },
    secondImage: {
      source: '',
      alternative: 'Второе изображение'
    },
    isFirstImageCorrect: true,
    prompt: '',
    voiceFile: ''
  };

  @Output() onCorrect = new EventEmitter();

  @Output() onIncorrect = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onImageClick(isFirstImage: boolean) {
    isFirstImage === this.quiz.isFirstImageCorrect ? this.onCorrect.emit() : this.onIncorrect.emit();
  }

}
