import { Component, OnInit, Input, Attribute } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() currentValue: number = 0;

  @Input() minValue: number = 0;

  @Input() maxValue: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
