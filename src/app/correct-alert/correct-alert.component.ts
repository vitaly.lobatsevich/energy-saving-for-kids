import { Component, Input } from '@angular/core';
import { Quiz } from '../entities/quiz';

export interface NextClickHandler {
  (): void;
}

@Component({
  selector: 'app-correct-alert',
  templateUrl: './correct-alert.component.html',
  styleUrls: ['./correct-alert.component.scss']
})
export class CorrectAlertComponent {

  @Input()
  isShown = false;

  @Input()
  quiz: Quiz | undefined | null;

  @Input()
  onNextClick: NextClickHandler = () => {};

  get prompt(): string {
    return this.quiz ? this.quiz.prompt : '';
  }

}
