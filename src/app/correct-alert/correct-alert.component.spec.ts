import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorrectAlertComponent } from './correct-alert.component';

describe('CorrectAlertComponent', () => {
  let component: CorrectAlertComponent;
  let fixture: ComponentFixture<CorrectAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorrectAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorrectAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
