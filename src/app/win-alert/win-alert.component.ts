import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-win-alert',
  templateUrl: './win-alert.component.html',
  styleUrls: ['./win-alert.component.scss']
})
export class WinAlertComponent {

  @Input()
  isShown = false;

}
