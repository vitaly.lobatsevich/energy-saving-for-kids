import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WinAlertComponent } from './win-alert.component';

describe('WinAlertComponent', () => {
  let component: WinAlertComponent;
  let fixture: ComponentFixture<WinAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WinAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WinAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
