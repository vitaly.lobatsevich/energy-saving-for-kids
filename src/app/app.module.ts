import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { PlayButtonComponent } from './play-button/play-button.component';
import { GameComponent } from './game/game.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { QuizComponent } from './quiz/quiz.component';
import { MainMenuButtonComponent } from './main-menu-button/main-menu-button.component';
import { CorrectAlertComponent } from './correct-alert/correct-alert.component';
import { IncorrectAlertComponent } from './incorrect-alert/incorrect-alert.component';
import { AlertComponent } from './alert/alert.component';
import { WinAlertComponent } from './win-alert/win-alert.component';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    PlayButtonComponent,
    GameComponent,
    ProgressBarComponent,
    QuizComponent,
    MainMenuButtonComponent,
    CorrectAlertComponent,
    IncorrectAlertComponent,
    AlertComponent,
    WinAlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
