import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncorrectAlertComponent } from './incorrect-alert.component';

describe('IncorrectAlertComponent', () => {
  let component: IncorrectAlertComponent;
  let fixture: ComponentFixture<IncorrectAlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncorrectAlertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncorrectAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
