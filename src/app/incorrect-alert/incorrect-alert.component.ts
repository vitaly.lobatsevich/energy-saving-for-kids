import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-incorrect-alert',
  templateUrl: './incorrect-alert.component.html',
  styleUrls: ['./incorrect-alert.component.scss']
})
export class IncorrectAlertComponent {

  @Input()
  isShown = false;

}
