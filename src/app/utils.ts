export interface FinalHandler {
    (): void;
}

export interface IAudio {
    play(): void;
    load(): void;
    pause(): void;
}

export function playSound(src: string): IAudio {
    const audio = new Audio();
    audio.src = src;
    audio.load();
    audio.play();
    return audio;
}
