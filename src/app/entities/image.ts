export interface Image {
    source: string,
    alternative: string
}
