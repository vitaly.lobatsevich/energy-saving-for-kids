import { Image } from './image';

export interface Quiz {
    firstImage: Image,
    secondImage: Image,
    isFirstImageCorrect: boolean,
    prompt: string,
    voiceFile: string,
}
