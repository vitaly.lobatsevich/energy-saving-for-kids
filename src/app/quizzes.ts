import { Quiz } from './entities/quiz';

export const QUIZZES: Quiz[] = [
    {
        firstImage: {
            source: 'assets/images/quizzes/bulb.webp',
            alternative: 'Первое изображение'
        },
        secondImage: {
            source: 'assets/images/quizzes/energy-saving-bulb.webp',
            alternative: 'Второе изображение'
        },
        isFirstImageCorrect: false,
        prompt: 'Используй энергосберегающие лампы. Они потребляют на 80% меньше энергии и служат в 8-10 раз '
              + 'дольше',
        voiceFile: 'assets/sounds/quizzes/1.wav'
    },
    {
        firstImage: {
            source: 'assets/images/quizzes/faucet-with-water.png',
            alternative: 'Первое изображение'
        },
        secondImage: {
            source: 'assets/images/quizzes/faucet.png',
            alternative: 'Второе изображение'
        },
        isFirstImageCorrect: false,
        prompt: 'Не забывай закрывать краны, когда не используешь воду',
        voiceFile: 'assets/sounds/quizzes/2.wav'
    },
    {
        firstImage: {
            source: 'assets/images/quizzes/bike.jpg',
            alternative: 'Первое изображение'
        },
        secondImage: {
            source: 'assets/images/quizzes/car.jpg',
            alternative: 'Второе изображение'
        },
        isFirstImageCorrect: true,
        prompt: 'Велосипеды помогут сберечь топливо и деньги. А ещё это полезно для здоровья!',
        voiceFile: 'assets/sounds/quizzes/3.wav'
    },
    {
        firstImage: {
            source: 'assets/images/quizzes/open-window.jpg',
            alternative: 'Первое изображение'
        },
        secondImage: {
            source: 'assets/images/quizzes/closed-window.png',
            alternative: 'Второе изображение'
        },
        isFirstImageCorrect: false,
        prompt: 'Закрытые окна помогут сберечь тепло в доме',
        voiceFile: 'assets/sounds/quizzes/4.wav'
    },
    {
      firstImage: {
          source: 'assets/images/quizzes/out-fork.png',
          alternative: 'Первое изображение',
      },
      secondImage: {
          source: 'assets/images/quizzes/in-fork.png',
          alternative: 'Второе изображение',
      },
      isFirstImageCorrect: true,
      prompt: 'Не забывай выключать из розетки неиспользуемые электроприборы',
      voiceFile: 'assets/sounds/quizzes/5.wav'
    },
    {
        firstImage: {
            source: 'assets/images/quizzes/fridge-with-ice.png',
            alternative: 'Первое изображение'
        },
        secondImage: {
            source: 'assets/images/quizzes/fridge.jpg',
            alternative: 'Второе изображение'
        },
        isFirstImageCorrect: false,
        prompt: 'Не забывай размораживать холодильник. Лёд внутри может вызвать повышенное потребление '
              + 'электроэнергии и привести к поломке прибора',
        voiceFile: 'assets/sounds/quizzes/6.wav'
    }
];
