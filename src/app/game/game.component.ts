import { Component, OnInit } from '@angular/core';
import { Quiz } from '../entities/quiz';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';
import { playSound } from '../utils';
import { IAudio } from '../utils';

interface IsShownSetter {
  (isShown: boolean): void
}

interface FinalHandler {
  (): void;
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  quizzes: Quiz[] = [];

  currentQuiz: Quiz | undefined;

  isClickable = true;

  isIncorrectAlertShown = false;

  isCorrectAlertShown = false;

  isWinAlertShown = false;

  currentVoicePrompt: IAudio | null = null;

  constructor(private quizService: QuizService, private router: Router) { }

  get numberOfQuizzes(): number {
    return this.quizzes.length;
  }

  get currentQuizNumber(): number {
    return this.quizzes.findIndex(quiz => quiz === this.currentQuiz);
  }

  setIsIncorrectAlertShown = (isIncorrectAlertShown: boolean) => this.isIncorrectAlertShown = isIncorrectAlertShown;

  setIsCorrectAlertShown = (isCorrectAlertShown: boolean) => this.isCorrectAlertShown = isCorrectAlertShown;

  setIsWinAlertShown = (isWinAlertShown: boolean) => this.isWinAlertShown = isWinAlertShown;

  ngOnInit(): void {
    this.getQuizzes();
  }

  getQuizzes() {
    this.quizService.getQuizzes()
      .subscribe(quizzes => {
        this.quizzes = quizzes;
        if (quizzes.length > 0) {
          this.currentQuiz = quizzes[0];
        }
      });
  }

  showAlert(setIsShown: IsShownSetter): void {
    if (this.isClickable) {
      this.isClickable = false;
      setIsShown(true);
    }
  }

  hideAlert(setIsShown: IsShownSetter): void {
    this.isClickable = true;
    setIsShown(false);
  }

  hideAlertAfterTimeout(setIsShown: IsShownSetter, onFinal: FinalHandler | undefined | null = undefined): void {
    setTimeout(() => {
      this.hideAlert(setIsShown);
      if (onFinal) {
        onFinal();
      }
    }, 3000);
  }

  playVoicePrompt(voiceFile: string): void {
    this.currentVoicePrompt?.pause();
    this.currentVoicePrompt = playSound(voiceFile);
  }

  onCorrect() {
    playSound('assets/sounds/success.wav');
    this.showAlert(this.setIsCorrectAlertShown);
    setTimeout(() => {
      if (this.currentQuiz)
      this.playVoicePrompt(this.currentQuiz.voiceFile);
    }, 1000);
  }

  onIncorrect() {
    playSound('assets/sounds/wrong.wav');
    this.showAlert(this.setIsIncorrectAlertShown);
    this.hideAlertAfterTimeout(this.setIsIncorrectAlertShown);
  }

  onNextClick = (): void => {
    this.hideAlert(this.setIsCorrectAlertShown);
    this.currentVoicePrompt?.pause();
    this.currentVoicePrompt = null;
    if (this.currentQuizNumber + 1 < this.quizzes.length) {
        this.currentQuiz = this.quizzes[this.currentQuizNumber + 1];
    } else {
      playSound('assets/sounds/win.wav');
      this.showAlert(this.setIsWinAlertShown);
      this.hideAlertAfterTimeout(this.setIsWinAlertShown, () => this.router.navigateByUrl('/main-menu'));
    }
  }

}
