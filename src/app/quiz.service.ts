import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Quiz } from './entities/quiz';
import { QUIZZES } from './quizzes';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor() { }

  getQuizzes(): Observable<Quiz[]> {
    return of(QUIZZES);
  }

}
